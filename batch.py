#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
import copy
from decimal import Decimal
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Eval, Bool
from trytond.wizard import Wizard
from trytond.modules.account_batch import BATCH_LINE_STATES, BATCH_LINE_DEPENDS
from trytond.pool import Pool
from trytond.transaction import Transaction


_ZERO = Decimal("0.0")

class Line(ModelSQL, ModelView):
    'Account Batch Entry Line'
    _name = 'account.batch.line'

    tax = fields.Many2One('account.tax', 'Tax', domain=[
            ('parent', '=', False)
            ], states=BATCH_LINE_STATES, depends=BATCH_LINE_DEPENDS)

    def __init__(self):
        super(Line, self).__init__()
        # Adding a provides mechanism for account_batch_invoice module
        # which could be installed, but must not depends
        if hasattr(self, 'invoice'):
            self.invoice = copy.copy(self.invoice)
            if self.invoice.states is None:
                self.invoice.states = {'readonly': Bool(Eval('tax'))}
                if 'tax' not in self.invoice.depends:
                    self.invoice.depends = copy.copy(self.invoice.depends)
                    self.invoice.depends.append('tax')
            # TODO: commented out due to #629
            #elif 'readonly'  in self.invoice.states:
            #    self.invoice.states['readonly'] = \
            #            self.invoice.states['readonly'] or \
            #            Bool(Eval('tax'))
            #else:
            #    self.invoice.states['readonly'] = Bool(Eval('tax'))

            self.tax = copy.copy(self.tax)
            if self.tax.states is None:
                self.tax.states = {'readonly': Bool(Eval('invoice'))}
                if 'invoice' not in self.tax.depends:
                    self.tax.depends = copy.copy(self.tax.depends)
                    self.tax.depends.append('invoice')
            # TODO: commented out due to #629
            #elif 'readonly'  in self.tax.states:
            #    self.tax.states['readonly'] = \
            #            self.tax.states['readonly'] or \
            #            Bool(Eval('invoice'))
            #else:
            #    self.tax.states['readonly'] = Bool(Eval('invoice'))

        self._reset_columns()
        self._error_messages.update({
            'same_account_and_contra': 'Account and contra account are ' \
                'equal!\nPlease provide different accounts.',
            'misconfigured_tax': 'Misconfigured Tax Error!',
            'misconfigured_tax_description': "The tax '%s' " \
                "is misconfigured!\nTaxes need to have defined base_codes " \
                "and tax_codes for the fiscal year.",
        })

    def on_change_contra_account(self, vals):
        account_obj = Pool().get('account.account')
        res = super(Line, self).on_change_contra_account(vals)
        if vals.get('contra_account'):
            account = account_obj.browse(vals['contra_account'])
            if account.taxes and len(account.taxes)==1:
                res['tax'] = account.taxes[0].id
        return res

    def _process_tax_line(self, tax, amount, sign_type='invoice'):
        currency_obj = Pool().get('currency.currency')

        if tax.type == 'percentage':
            if tax.reverse_charge:
                amount = amount * tax.percentage / Decimal('100')
            else:
                amount = amount / (1 + Decimal('100') / abs(tax.percentage))
                if tax.percentage < 0:
                    amount *= -1
        elif tax.type == 'fixed':
            amount = tax.amount

        amount = currency_obj.round(tax.company.currency, amount)

        res_tax_line = False
        if amount != _ZERO:
            res_tax_line = ('create', {
                'currency_digits': tax.company.currency.digits,
                'amount': amount * tax[sign_type + '_tax_sign'],
                'code': tax.invoice_tax_code.id or False,
                'tax': tax.id,
                })
        return (amount, res_tax_line)

    def _process_base_line(self, tax, base_amount, sign_type='invoice'):
        res_base_line = False
        if base_amount != _ZERO:
            res_base_line = ('create', {
                'currency_digits': tax.company.currency.digits,
                'amount': base_amount * tax[sign_type + '_base_sign'],
                'code': tax.invoice_base_code.id,
                'tax': tax.id,
                })
        return res_base_line

    def _process_move_line(self, batch_line_vals, tax, tax_amount):
        journal_obj = Pool().get('account.batch.journal')
        batch_journal = journal_obj.browse(batch_line_vals['journal'])
        _, side = self._choose_account(batch_line_vals['amount'],
            batch_journal.account_journal)

        is_cancelation_move = batch_line_vals.get('is_cancelation_move')
        if tax_amount < _ZERO:
            # Check this if it is correct!!!!
            side = self._opposite(side)
            if not is_cancelation_move:
                tax_amount = abs(tax_amount)
        elif tax_amount > _ZERO and is_cancelation_move:
            if tax.percentage > 0:
                side = self._opposite(side)
            tax_amount *= Decimal('-1')

        res_move_line = {
            'name': batch_line_vals.get('posting_text') or '-',
            'debit': side == 'credit' and tax_amount or _ZERO,
            'credit': side == 'debit' and tax_amount or _ZERO,
            'account': tax.invoice_account.id,
            'party': batch_line_vals.get('party', False),
            'reference': batch_line_vals.get('sequence') or '',
            'external_reference': batch_line_vals.get(
                'external_reference') or '',
            #'second_currency': second_currency,
            #'amount_second_currency': tax_amount_second_currency,
            }
        return res_move_line

    def _get_move_lines(self, batch_line_vals):
        '''
        Return the values of the move lines for the batch line

        :param batch_line: a BrowseRecord of the batch line
        :return: a list of dictionary of move line values
        '''
        journal_obj = Pool().get('account.batch.journal')

        res = super(Line, self)._get_move_lines(batch_line_vals)
        vals = copy.copy(res)

        # Create tax account move line
        if batch_line_vals.get('tax'):
            side = self._get_move_tax_side(batch_line_vals)
            for i, move_line in enumerate(vals):
                # Must be checked if all this works with side == 'account'!!!
                if move_line['account'] == batch_line_vals[side]:
                    (net_amount, new_tax_lines, new_move_lines) = \
                            self._get_lines_tax(batch_line_vals)
                    if not (new_move_lines or new_tax_lines):
                        continue
                    for line in new_move_lines:
                        vals.append(line)
                    vals[i].update({'tax_lines': new_tax_lines})
                    if self.hook_tax_set_debit_credit(batch_line_vals):
                        is_cancelation_move = batch_line_vals.get(
                            'is_cancelation_move') or False
                        batch_journal = journal_obj.browse(
                            batch_line_vals['journal'])
                        _, side = self._choose_account(
                            batch_line_vals['amount'],
                            batch_journal.account_journal)
                        if side == 'credit' and not is_cancelation_move:
                            vals[i]['debit'] = net_amount
                        elif side == 'debit' and is_cancelation_move:
                            vals[i]['debit'] = net_amount
                        else:
                            vals[i]['credit'] = net_amount
                    break
        return vals

    def hook_tax_set_debit_credit(self, batch_line):
        '''A hook to control the setting of debit and credit.'''
        return True

    def _get_lines_tax(self, batch_line_vals):
        pool = Pool()
        currency_obj = pool.get('currency.currency')
        journal_obj = pool.get('account.batch.journal')
        res_move_lines = []
        res_tax_lines = []
        sign_type = self._choose_tax_sign_type(batch_line_vals)

        journal = journal_obj.browse(batch_line_vals['journal'])
        from_currency = journal.currency
        to_currency = journal.company.currency
        with Transaction().set_context(date=batch_line_vals['date']):
            amount = currency_obj.compute(from_currency.id,
                abs(batch_line_vals['amount']), to_currency.id)

        amount = abs(amount)
        is_cancelation_move = batch_line_vals.get('is_cancelation_move')
        if is_cancelation_move:
            amount *= Decimal('-1')
        net_amount = False
        for tax in self._get_taxes(batch_line_vals['tax']):
            if tax.invoice_tax_code and amount != _ZERO:
                (tax_amount, tax_line) = self._process_tax_line(tax, amount,
                        sign_type=sign_type)
                if not tax_amount:
                    return (None, None, None)
                tax_move_line = self._process_move_line(batch_line_vals, tax,
                        tax_amount)
                if is_cancelation_move:
                    tax_line[1]['amount'] *= Decimal('-1')
                tax_move_line['tax_lines'] = [tax_line]
                res_move_lines.append(tax_move_line)

                if not net_amount:
                    if not tax.reverse_charge:
                        net_amount = abs(amount) - abs(tax_amount)
                    else:
                        net_amount = abs(amount)
                    if is_cancelation_move:
                        net_amount *= Decimal('-1')

            # Add base lines
            if tax.invoice_base_code and amount:
                if not net_amount:
                    net_amount = amount
                base_line = self._process_base_line(tax, net_amount,
                        sign_type=sign_type)
                if is_cancelation_move:
                    base_line[1]['amount'] *= Decimal('-1')
                res_tax_lines.append(base_line)

        return (net_amount, res_tax_lines, res_move_lines)

    def _get_taxes(self, tax):
        tax_obj = Pool().get('account.tax')

        res = []
        if isinstance(tax, (int, long)):
            tax = tax_obj.browse(tax)
        if tax.type != 'none':
            res.append(tax)
        if tax.childs:
            for child in tax.childs:
                res.extend(self._get_taxes(child))
        return res

    def _choose_tax_sign_type(self, batch_line_vals):
        pool = Pool()
        account_obj = pool.get('account.account')
        batch_journal_obj = pool.get('account.batch.journal')

        amount = batch_line_vals['amount']
        if amount >= _ZERO:
            res = 'invoice'
        else:
            res = 'credit_note'

        contra_account = account_obj.browse(batch_line_vals['contra_account'])
        batch_journal = batch_journal_obj.browse(batch_line_vals['journal'])
        type_ = batch_journal.account_journal.type

        if type_ in ['cash', 'bank', 'revenue']:
            # TODO: use _get_move_tax_side instead of 'contra_account'
            if contra_account.kind in ['expense', 'other']:
                if amount < _ZERO:
                    res = 'invoice'
                else:
                    res = 'credit_note'
        elif type_ in ['expense', 'general', 'situation']:
            # TODO: use _get_move_tax_side instead of 'contra_account'
            if contra_account.kind in ['revenue', 'other']:
                if amount < _ZERO:
                    res = 'invoice'
                else:
                    res = 'credit_note'
        return res

    def _get_move_tax_side(self, batch_line_vals):
        # not yet implemented
        return 'contra_account'

Line()


class CancelBatchLines(Wizard):
    'Cancel Batch Lines'
    _name = 'account.batch.cancel'

    def _process_cancelation_values(self, batch_line_id):
        batch_line_obj = Pool().get('account.batch.line')
        if isinstance(batch_line_id, (list)):
            batch_line_id = batch_line_id[0]

        res = super(CancelBatchLines, self)._process_cancelation_values(
                batch_line_id)

        batch_line = batch_line_obj.browse(batch_line_id)
        if not batch_line.tax:
            return res

        res = res.copy()
        res['tax'] = batch_line.tax.id
        return res

CancelBatchLines()

