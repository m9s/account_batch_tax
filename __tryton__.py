#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Account Batch Tax',
    'name_de_DE': 'Buchhaltung Stapelbuchung Steuer',
    'version': '2.2.0',
    'author': 'virtual-things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''Tax for Batch Entries
    - Adds taxes to the batch entry line.
    - When using amount and tax together, the amount is including the tax.
    - All calculations of net amount and tax amount are done automatically
      on validating the batch.
''',
    'description_de_DE': '''Steuer für die Stapelbuchung
    - Fügt Steuern auf der Stapelbuchungszeile hinzu.
    - Bei gleichzeitigem Gebrauch von Betrag und Steuer ist die Steuer im Betrag enthalten.
    - Sämtliche Berechnungen für Netto- und Steuerbeträge werden automatisch bei der Prüfung
      des Buchungsstapels durchgeführt.
''',
    'depends': [
        'account_batch',
        'account_tax_reverse_charge',
    ],
    'xml': [
        'batch.xml',
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
